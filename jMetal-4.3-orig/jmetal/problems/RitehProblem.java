//  RitehProblem.java
//
//  Author:
//       Antonio J. Nebro <antonio@lcc.uma.es>
//       Juan J. Durillo <durillo@lcc.uma.es>
//
//  Copyright (c) 2011 Antonio J. Nebro, Juan J. Durillo
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Lesser General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Lesser General Public License for more details.
// 
//  You should have received a copy of the GNU Lesser General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.

package jmetal.problems;

import jmetal.core.*;
import jmetal.util.JMException;
import jmetal.encodings.solutionType.RealIntIntSolutionType;
/**
 * Class representing problem RitehProblem
 */
public class RitehProblem extends Problem {  
	int a = 0;
        int realVariables_ ;
        int intVariables_  ;
        int intVariables2_  ;
	private static final String COMMA_DELIMITER = ",";
    
  /** 
   * Constructor.
   * Creates a default instance of the RitehProblem problem.
   * @param solutionType The solution type must "Real", "BinaryReal, and "ArrayReal".
     * @param met 
     * @throws java.lang.ClassNotFoundException 
   */
  public RitehProblem(String solutionType, String[][] met) throws ClassNotFoundException {
    this(solutionType, met, 2397, 50, 50, 50);
  } // RitehProblem, ovo je samo ako se ne postave svi argumenti
  
  /** 
   * Constructor.
   * Creates a new instance of the RitehProblem problem.
   * @param solutionType The solution type must "Real", "BinaryReal, and "ArrayReal".
     * @param metrics 
     * @param realVariables 
     * @param intVariables 
     * @param intVariables2 
     * @param brojDatoteka 
     * @throws java.lang.ClassNotFoundException 
   */
  public RitehProblem(String solutionType, String[][] metrics, int brojDatoteka, int realVariables, int intVariables, int intVariables2) throws ClassNotFoundException {
	
	numberOfObjectives_  = brojDatoteka               ; //fitness funkcija (najprije s 1 (apsolutna razlika) a poslije s 2(relativna razlika))
        numberOfConstraints_ = 0                            ;
        problemName_         = "RitehProblem"               ;
        realVariables_ = realVariables ;
        intVariables_  = intVariables  ;
        intVariables2_  = intVariables2  ;
        numberOfVariables_   = realVariables_+intVariables_ + intVariables2_;
        
        brojDatoteka_ = brojDatoteka; 
        setMetrics(brojDatoteka, numberOfVariables_/3);
        
    //Solution.goal_ = new double [brojDatoteka];
   System.out.println(numberOfVariables_) ;
    for(int i=0; i<brojDatoteka; i++){
            System.arraycopy(metrics[i], 1, metrics_[i], 1, numberOfVariables_/3-1);
    }
  
    upperLimit_ = new double[numberOfVariables_] ;
    lowerLimit_ = new double[numberOfVariables_] ;
       
    for (int i = 0; i < realVariables_; i++) {
        lowerLimit_[i] = -1000.0 ;
        upperLimit_[i] = 1000.0  ;
    } // for
    
    for (int i = realVariables_; i < (realVariables_+ intVariables_); i++) {
        lowerLimit_[i] = 1 ;
        upperLimit_[i] = 4  ;
    } // for
        
    for (int i = (realVariables_+ intVariables_); i < (realVariables_+ intVariables_+ intVariables2_); i++) {
        lowerLimit_[i] = 1 ;
        upperLimit_[i] = 49 ;
    } // for
        
    if (solutionType.compareTo("RealIntInt") == 0)
    	solutionType_ = new RealIntIntSolutionType(this, realVariables, intVariables,  intVariables2) ;
    else {
    	System.out.println("Error: solution type " + solutionType + " invalid") ;
    	System.exit(-1) ;
    }
  } // RitehProblem
    
  /** 
  * Evaluates a solution 
  * @param solution The solution to evaluate
   * @throws JMException 
  */
        @Override
  public void evaluate(Solution solution) throws JMException {
    Variable[] variable = solution.getDecisionVariables();
       
    double [] fx = new double [brojDatoteka_] ; for(int i=0;i<fx.length;i++)fx[i] = 0;// function values     
    double [] x_real = new double[realVariables_] ;    
    int [] x_int = new int[intVariables_] ;    
    int [] x_int2 = new int[intVariables2_] ;
    double [][] operands = new double[brojDatoteka_][numberOfVariables_/3];
    double [] temp= new double[numberOfVariables_];for(int i=0;i<temp.length;i++)temp[i] = 0;

    int [] operators = new int[numberOfVariables_/3]; // 0 = + // 1 = - // 2 = * // 3 = /
    int [] bug_cnt = new int [brojDatoteka_];
    int [] ff = new int [brojDatoteka_];
    
    double TruePositive=0;
    double TrueNegative=0;
    double FalseNegative=0;
    double FalsePositive=0;
    double brojac=0;
        
    for (int var = 0; var < realVariables_ ; var++) {        
        x_real[var] = variable[var].getValue() ;
        //System.out.println("Real"+var+":"+x_real[var]);
    } // for   
    for (int var = realVariables_; var < (realVariables_+intVariables_) ; var++) {        
        x_int[var-realVariables_] = (int)variable[var].getValue() ;
        //System.out.println("Int"+var+":"+x_int[var-50]);
    } // for    
    
    for (int var = (realVariables_+intVariables_); var < (realVariables_+intVariables_+intVariables2_) ; var++) {        
        x_int2[var-(realVariables_+intVariables_)] = (int)variable[var].getValue() ;
        //System.out.println("Int2-"+var+":"+x_int2[var-100]);
    } // for  
   
      
 //RANDOMIZIRANJE POLJA
        int[] duplicates_pos=new int[intVariables2_]; for(int i=0;i<duplicates_pos.length;i++)duplicates_pos[i]=0; 
        int[] missing=new int[intVariables2_]; for(int i=0;i<missing.length;i++)missing[i]=1;
        
        for(int i=1;i<x_int2.length-1;i++)
        {
            for(int j=i+1;j<x_int2.length-1;j++)
            {
                if(j!=i && x_int2[i]==x_int2[j]) duplicates_pos[j]=1;   
            }
            
            for(int j=1;j<x_int2.length-1;j++)
            {
                if(j==x_int2[i]) missing[j]=0;
            }
        }
        
        //for(int i=0;i<duplicates_pos.length;i++)System.out.println(i+" - ovovo - " +duplicates_pos[i]+" - a ovo je missing"+missing[i]);
        
         for(int i=1;i<x_int2.length-1;i++)
         {
             if (duplicates_pos[i]==1)
             {
                 for(int j=1;j<x_int2.length-1;j++)
                 {
                     if(missing[j]==1) {x_int2[i]=j; missing[j]=0; break;}
                 }
             }
         }

        /*ISPIS
         for(int i=1;i<x_int2.length-1;i++)
         {
             System.out.println(brojac+"Ovo je konacno polje - "+x_int2[i]);
             brojac++;
             
         }*/
// ------------------------------------------------------------------------         
        
        for(int i=0; i<brojDatoteka_; i++)
        {
            for(int j = 1; j<(numberOfVariables_/3-1); j++)
            {
                
                operands[i][j] = x_real[j-1] * getMetric(i, x_int2[j]);

            }
            for(int j = 1; j<numberOfVariables_/3; j++)
            {
                if(j==47) break;
                if(x_int[j-1]==1) operators[j]=0;
                else if(x_int[j-1]==2) operators[j]=1;
                else if(x_int[j-1]==3) operators[j]=2;
                else if(x_int[j-1]==4) operators[j]=3;

            }
        }
        
//IZRACUNAVANJE        
        for(int i=0; i<brojDatoteka_; i++)
        {
            for(int j = 1; j<numberOfVariables_/3; j++)
            {   
                //PLUS I MINUS
                if(operators[j]==0 || operators[j]==1)
                {
                    if(j!=1) 
                    {
                    if(operators[j-1]==0){        fx[i] += operands[i][j];}
                    else if(operators[j-1]==1){   fx[i] -= operands[i][j]; }
                    }
                    else{ fx[i]=operands[i][j];}
                }
                
                //MNOZENJE
                else if(operators[j]==2)
                {
                  temp[j]=operands[i][j]*operands[i][j+1];
                  int z = j+1;

                  while (operators[z]==2 || operators[z]==3)
                  {
                        if(operators[z]==2)
                        {
                            temp[j] *= operands[i][z+1];
                        }
                        else if(operators[z]==3)
                        {
                           if(operands[i][z+1]!=0)
                            {
                              temp[j] /= operands[i][z+1];
                            }
                        }
                    z++;
                  }
                  if(operators[j-1]==1)
                  {
                    temp[j]=-temp[j];          
                  }
                  j=z;
                }
                
                //DIJELJENJE
                else if(operators[j]==3)
                {
                    if(operands[i][j+1]==0)
                    {
                      temp[j]=0;
                    }else
                    {
                        temp[j]=operands[i][j]/operands[i][j+1];
                    }
                    
                    int z = j+1;
                    while (operators[z]==2 || operators[z]==3)
                    {
                      if(operators[z]==2)
                      {
                            temp[j] *= operands[i][z+1];
                      }
                      else if(operators[z]==3)
                      {
                            if(operands[i][z+1]!=0)
                            {
                              temp[j] /= operands[i][z+1];
                            }
                      }
                      z++;
                  }

                  if(operators[j-1]==1)
                  {
                    temp[j]=-temp[j];          
                  }
                  j=z;
                }
            }
            
        for(int c=0;i<temp.length;i++)
        {
          fx[i]+=temp[c];
        }

        //BUG_CNT
        if(getMetric(i,49)==0)
        {
            bug_cnt[i]=0;
        }
        else if (getMetric(i,49)>0)
        {
            bug_cnt[i]=1;
        }
        
        //FX iz NSGA-II
        if(fx[i]<0)
        {
            ff[i]=0;
        }
        else 
        {
            ff[i]=1;
        }
        //System.out.println("broj fx: " + fx[i] +" datoteka: "+i + " -----ff: "+ff[i]+" bug_cnt: "+bug_cnt[i]);
        //System.out.println("pravi bug count: " + getMetric(i,49));
        
        
        if(bug_cnt[i]==0 && ff[i]==0){ TrueNegative++;}
        if(bug_cnt[i]==1 && ff[i]==1){ TruePositive++;}
        if(bug_cnt[i]==1 && ff[i]==0){ FalseNegative++;}
        if(bug_cnt[i]==0 && ff[i]==1){ FalsePositive++;}
        
        brojac++;
       a++;
    }
    double TPR = TruePositive/(TruePositive+FalseNegative);
    double TNR = TrueNegative/(TrueNegative+FalsePositive);
    //System.out.println("\nBrojac: " + a);
    System.out.println("\n\nTrueNegative: " + TrueNegative+" TruePositive: "+ TruePositive+" FalseNegative: "+FalseNegative+" FalsePositive: "+FalsePositive);
    System.out.println(" TRUE POSITIVE RATE: "+ TPR +" |  TRUE NEGATIVE RATE: " + TNR );

    
    
    solution.setObjective(0, (1-TPR)); 
    solution.setObjective(1, (1-TNR)); 
    
  } // evaluate
} // RitehProblem
/*
FUN zapisuje fitness function, stalno se ponovno izvrsava 
VAR zapisuje X-eve odnosno najbolju jedinku/najbolji array X
----izvrsi se 5 puta/evaluacija po 5 datoteka 
---- broj varijabli je 49, prvu zanemarimo jer je ime datoteke, a zadnja je bug_count
*/