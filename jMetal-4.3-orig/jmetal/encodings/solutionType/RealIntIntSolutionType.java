/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jmetal.encodings.solutionType;

import jmetal.core.Problem;
import jmetal.core.SolutionType;
import jmetal.core.Variable;
import jmetal.encodings.variable.Int;
import jmetal.encodings.variable.Real;

/**
 *
 * @author cop24
 */
public class RealIntIntSolutionType extends SolutionType{
    
	private int realVariables_ ;
    	private int intVariables_ ;
	private int intVariables2_ ;

	/**
	 * Constructor
	 * @param problem
	 * @param intVariables2 Number of integer variables
	 * @param intVariables Number of integer variables
	 * @param realVariables Number of real variables
	 * @throws ClassNotFoundException 
	 */
	public RealIntIntSolutionType(Problem problem, int realVariables, int intVariables, int intVariables2) throws ClassNotFoundException {
		super(problem) ;
		realVariables_ = realVariables ;
		intVariables_ = intVariables ;
		intVariables2_ = intVariables2 ;
	} // Constructor

	/**
	 * Creates the variables of the solution
	 * @param decisionVariables
	 * @throws ClassNotFoundException 
	 */
	public Variable[] createVariables() throws ClassNotFoundException {
		Variable [] variables = new Variable[problem_.getNumberOfVariables()];
                
                for (int var = 0; var < realVariables_; var++)
                    variables[var] = new Real(problem_.getLowerLimit(var), problem_.getUpperLimit(var));  

		for (int var = realVariables_; var < (realVariables_+intVariables_); var++)
                    variables[var] = new Int((int)problem_.getLowerLimit(var), (int)problem_.getUpperLimit(var)); 
		
		for (int var = (realVariables_+intVariables_); var < (realVariables_+intVariables_+intVariables2_); var++)
                    variables[var] = new Int((int)problem_.getLowerLimit(var), (int)problem_.getUpperLimit(var)); 
		
		return variables ;
	} // createVariables
}
