/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package classification;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class classification {

  public static void main(String [] args) throws FileNotFoundException{

    int brojDatoteka= 1000;
    int numberOfVariables= 50;
    
    double [] fx = new double [2000]; for(int i=0; i<fx.length; i++) fx[i] = 0;// function values   
    double [][] operands = new double[2000][50];
    double [] temp= new double[numberOfVariables]; for(int i=0;i<temp.length;i++)temp[i] = 0;
    
    double [][] x_real = new double[2000][50] ;    
    int [][] x_int = new int[2000][50] ;    
    int [][] x_int2 = new int[2000][50] ;
    double [][] fun = new double[2000][2] ;   
    int [] fun_weight= new int[2000];
    
    
    int [] operators = new int[numberOfVariables]; // 0 = + // 1 = - // 2 = * // 3 = /
    int [] ff = new int [2000];
    int operator_place;
    int pogodak=0,truepogodak=0,falsepogodak=0,truebrojac=0,falsebrojac=0;
        
     //UCITAVANJE ZA PODATKE IZ VAR DATOTEKE
    Scanner scanner = null; 
    String[] line = new String [2000];
    String[][] var_string = new String[2000][100];
    String[][] fun_string = new String[2000][100];
    int var_l =0;
    int fun_l =0;
        
    scanner = new Scanner(new File("VAR"));
    //Use Delimiter as COMMA
    scanner.useDelimiter("\n");
    while(scanner.hasNext()){
        line[var_l] = scanner.next();
        var_l++;
    }

    for(int i = 0; i< var_l; i++) var_string[i] = line[i].split(" ");
    
    //UCITAVANJE IZ FUNA           
    scanner = new Scanner(new File("FUN"));
    //Use Delimiter as COMMA
    scanner.useDelimiter("\n");
    while(scanner.hasNext()){
        line[fun_l] = scanner.next();
        fun_l++;
    }

    for(int i = 0; i< var_l; i++) fun_string[i] = line[i].split(" ");
    
    for(int i=0;i<fun_l;i++){
        fun[i][0]=Double.parseDouble(fun_string[i][0]);
        fun[i][1]=Double.parseDouble(fun_string[i][1]);
        if( (1-fun[i][0]>0.5) && (1-fun[i][1]>0.5) ) {
            fun_weight[i]=2;
        }else{
            fun_weight[i]=1;
        }
    }
    
    //#########################################################################
    //UCITAVANJE ZA (NOVE) METRIKE     
    String[][] metrics_string = new String[brojDatoteka][50];
    int l =0;
    scanner = new Scanner(new File("C:\\Users\\cop24\\Desktop\\UPI 2017\\nsgaii_new\\JDT_R2_1.csv"));
    //Use Delimiter as COMMA
    scanner.useDelimiter("\n");
    scanner.next();
    while(scanner.hasNext() && l<brojDatoteka){
        line[l] = scanner.next();
        l++;
    }
        
    for(int i = 0; i< brojDatoteka; i++) metrics_string[i] = line[i].split(",");
   
    double[][] metrics = new double[brojDatoteka][50];
    double[][] var = new double[var_l][100];
    for(int i=0;i<brojDatoteka;i++){
        for(int j=1;j<50;j++){
            metrics[i][j] = Double.parseDouble(metrics_string[i][j]);
        }  
    }
    
    for(int i=0;i<var_l;i++){
        for(int j=0;j<50;j++){
            x_real[i][j]=Double.parseDouble(var_string[i][j]);
            //System.out.println("x_real["+i+"]["+j+"]:"+x_real[i][j]);
        }
        for(int j=50;j<100;j++){
            x_int[i][j-50]=Integer.parseInt(var_string[i][j]);
            //System.out.println("x_int["+i+"]["+j+"]:"+x_int[i][j-50]);
        }  
        for(int j=100;j<150;j++){
            x_int2[i][j-100]=Integer.parseInt(var_string[i][j]);
            //System.out.println("x_int2["+i+"]["+j+"]:"+x_int2[i][j-100]);
        }  
    }
      
    int[] duplicates_pos=new int[50]; for(int i=0;i<duplicates_pos.length;i++)duplicates_pos[i]=0; 
    int[] missing=new int[50]; for(int i=0;i<missing.length;i++)missing[i]=1;
    for(int k=0; k<brojDatoteka; k++)
    {
        for(int i=1;i<50-1;i++)
        {
            for(int j=i+1;j<50-1;j++)
            {
                if(j!=i && x_int2[k][i]==x_int2[k][j]) duplicates_pos[j]=1;   
            }
            
            for(int j=1;j<x_int2.length-1;j++)
            {
                if(j==x_int2[k][i]) missing[j]=0;
            }
        }
        
        //for(int i=0;i<duplicates_pos.length;i++)System.out.println(i+" - ovovo - " +duplicates_pos[i]+" - a ovo je missing"+missing[i]);
        
         for(int i=1;i<50-1;i++)
         {
             if (duplicates_pos[i]==1)
             {
                 for(int j=1;j<50-1;j++)
                 {
                     if(missing[j]==1) {x_int2[k][i]=j; missing[j]=0; break;}
                 }
             }
         }
    }
    
    for(int k=0; k<brojDatoteka; k++){ //FOR ZA DATOTEKE

        int brojJedinica=0;
        int brojNula=0;
        for(int i=0; i<var_l; i++){
            for(int j = 1; j<49; j++){
                int pos=x_int2[i][j];
                operands[i][j] = x_real[i][j-1] * metrics[k][pos];
                //System.out.println(operands[i][j]);
            }
            for(int j = 1; j<47; j++)
            {
                if(x_int[i][j-1]==1) operators[j]=0;
                else if(x_int[i][j-1]==2) operators[j]=1;
                else if(x_int[i][j-1]==3) operators[j]=2;
                else if(x_int[i][j-1]==4) operators[j]=3;
            }
        }

        //IZRACUNAVANJE

        for(int i=0; i<var_l; i++){ //FOR ZA REDAK VAR VRIJEDNOSTI
            for(int j = 1; j<50; j++){ //FOR ZA VAR VRIJEDNOSTI

                operator_place=j+49;
                if(operator_place==98) break;

                if(operators[j]==0 || operators[j]==1) {
                    if(j!=1){
                        if(operators[j-1]==0)fx[i] += operands[i][j];
                        else if(operators[j-1]==1)fx[i] -= operands[i][j];
                    }
                    else{ 
                        fx[i]=operands[i][j];
                    }
                }

                //MNOZENJE
                else if(operators[j]==2){
                    temp[j]=operands[i][j]*operands[i][j+1];
                    int z = j+1;
                    while (operators[z]==2 || operators[z]==3){
                          if(operators[z]==2){
                              temp[j] *= operands[i][z+1];
                          }
                          else if(operators[z]==3){
                            if(operands[i][z+1]!=0)
                            {
                              temp[j] /= operands[i][z+1];
                            }
                          }
                      z++;
                    }
                    if(operators[j-1]==1){
                      temp[j]=-temp[j];          
                    }
                    j=z;
                }
                //DIJELJENJE
                else if(operators[j]==3){
                    if(operands[i][j+1]==0){
                        temp[j]=0;
                    }
                    else{
                        temp[j]=operands[i][j]/operands[i][j+1];
                    }
                    int z = j+1;

                    while (operators[z]==2 || operators[z]==3)
                    {
                        if(operators[z]==2){
                            temp[j] *= operands[i][z+1];
                        }
                        else if(operators[z]==3){
                            if(operands[i][z+1]!=0)
                            {
                              temp[j] /= operands[i][z+1];
                            }
                        }
                        z++;
                    }
                    if(operators[j-1]==1){
                      temp[j]=-temp[j];          
                    }
                    j=z;
                }
            }//FOR ZA VAR VRIJEDNOSTI

            for(int c=0;c<temp.length;c++){
                fx[i]+=temp[c];
            }
            for(int c=0;c<temp.length;c++){
                temp[c]=0;
            }
            //FX iz NSGA-II
            if(fx[i]<0){
                ff[i]=0;
                brojNula+=fun_weight[i];
            }
            else{
                ff[i]=1;
                brojJedinica+=fun_weight[i];
            }
            //System.out.println("i: "+i+ " broj fx: " + fx[i] +" datoteka: "+i + " -----ff: "+ff[i] + "BrojJedinica:" +brojJedinica + "BrojNula:"+brojNula);
            //System.out.println("pravi bug count: " + getMetric(i,49));
        }//FOR ZA VAR REDAK
        
        if(brojJedinica>brojNula)
        {
             System.out.println("FILE: "+ metrics_string[k][0] +"\tIMA gresaka. | BUG_CNT: " + metrics_string[k][49]);
             if(metrics[k][49]>0){ pogodak++;truepogodak++;}
        }else
        {
             System.out.println("FILE: "+ metrics_string[k][0] +"\tNEMA gresaka. | BUG_CNT: " + metrics_string[k][49]);
             if(metrics[k][49]==0){ pogodak++;falsepogodak++;}
        }
        
        if(metrics[k][49]>0) truebrojac++;
        else if (metrics[k][49]==0) falsebrojac++;
        //System.out.println("\n\n Broj pogodaka: "+pogodak);
    }
    float postotak=(float)pogodak/(float)brojDatoteka;
    float truepostotak=(float)truepogodak/(float)truebrojac;
    float falsepostotak=(float)falsepogodak/(float)falsebrojac;
    System.out.println("\n BROJ POGODAKA: "+pogodak +" / "+brojDatoteka+"\n POSTOTAK: "+postotak);
    System.out.println("\n POSTOTAK točno pogođenih datoteka SA greškom("+truepogodak+") : " +truepostotak);
    System.out.println(" POSTOTAK točno pogođenih datoteka BEZ greške("+falsepogodak+") : " +falsepostotak);
    }
}