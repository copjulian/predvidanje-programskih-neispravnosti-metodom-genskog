# README #


### What is this repository for? ###

Repozitorij za projekt pod nazivom "Predviđanje programskih neispravnosti metodom genskog programiranja" u sklopu kolegija "Upravljanje u programskom inžinjerstvu". 

### How do I get set up? ###

Jedina posebna napomena prilikom pokretanja programa jest da je unutar datoteke NSGAII_main.java te classification.java potrebno promjeniti putanju koja pokazuje na csv datoteku. Pri tome se za NSGAII_main.java postavljaju podaci (dataset) za koje mislimo da bi trebali biti ulazni, dok za classification.java treba postaviti testne podatke. 
Program za učenje i program za klasifikaciju pokreću se zasebno.